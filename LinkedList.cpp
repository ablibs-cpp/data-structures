#include "LinkedList.h"
#include <vector>

using namespace std;

LinkedList::LinkedList()
{
    head = NULL;
    tail = NULL;
    sum = 0;
    average = 0;
    count = 0;
}

LinkedList::~LinkedList()
{
    delete head;
    delete tail;
}
    Node* LinkedList::addAfter( Node * qNode, int value )
    {
        Node* temp = head;
        while( temp != NULL )
        {
            if( temp == qNode )
            {

                if( qNode == tail )
                {
                    return LinkedList::addLast( value );
                }
                else
                {
                    Node* newNode = new Node();

                    newNode->value = value;
                    newNode->nextNode = qNode->nextNode;
                    newNode->prevNode = qNode;
                    newNode->nextNode->prevNode = newNode;
                    newNode->prevNode->nextNode = newNode;

                    sum += value;
                    ++count;
                    return newNode;
                }
            }
            temp = temp->nextNode;
        }
    }

    Node* LinkedList::AddBefore( Node * qNode, int value )
    {
        Node* temp = head;
        while( temp->nextNode != NULL )
        {
            if( temp->nextNode == qNode )
            {
                if( qNode == head )
                {
                    return LinkedList::addFirst( value );
                }
                else
                {
                    Node* newNode = new Node();

                    newNode->value = value;
                    newNode->nextNode = qNode;
                    newNode->prevNode = qNode->prevNode;
                    newNode->nextNode->prevNode = newNode;
                    newNode->prevNode->nextNode = newNode;


                    sum += value;
                    ++count;
                    return newNode;
                }
            }
            temp = temp->nextNode;
        }
    }

    Node* LinkedList::addLast( int value )
    {
        Node* newNode = new Node();
        newNode->value = value;

        if( head == NULL )
        {
            head = newNode;
            tail = newNode;
        }
        else
        {
            tail->nextNode = newNode;
            newNode->prevNode = tail;
            tail = newNode;
        }

        sum += value;
        ++count;
        return newNode;
    }

    void LinkedList::addLast( Node* node )
    {
        if( head == NULL )
        {
            head = node;
            tail = node;
        }
        else
        {
            tail->nextNode = node;
            node->prevNode = tail;
            tail = node;
        }

        sum += node->value;
        ++count;
    }

    void LinkedList::addRange( LinkedList linkedList )
    {
        if(linkedList.head != NULL )
        {
            tail->nextNode = linkedList.head;
            linkedList.head->prevNode = tail;
            tail = linkedList.tail;
            count += linkedList.count;
            sum += linkedList.sum;
        }
    }

    Node** LinkedList::find( int value )
    {
        Node* temp = head; 
        Node** res = new Node*[count];

        int i = 0;
        while( temp != NULL )
        {
            if( temp->value == value )
            {
                res[i++] = temp;
            }
            temp = temp->nextNode;
        }
        return res;
    }

    Node* LinkedList::findFirst( int value )
    {
        Node* temp = head;

        while( temp != NULL )
        {
            if( temp->value == value )
            {
                break;
            }
            temp = temp->nextNode;
        }
        return temp;
    }

    Node * LinkedList::findLast( int value )
    {
        Node* temp = tail;

        while( temp != NULL )
        {
            if( temp->value == value )
            {
                break;
            }
            temp = temp->prevNode;
        }
        return temp;
    }

    void LinkedList::removeFirst()
    {
        Node* temp = head;

        head = head->nextNode;
        head->prevNode = NULL;
        sum -= temp->value;
        --count;
        delete temp;
    }

    void LinkedList::removeLast()
    {
        Node* temp = tail;

        tail = tail->prevNode;
        tail->nextNode = NULL;
        sum -= temp->value;
        --count;
        delete temp;
    }

    Node* LinkedList::addFirst( int value )
    {
        Node* newNode = new Node();
        newNode->value = value;

        if( head == NULL )
        {
            head = newNode;
            tail = newNode;
        }
        else
        {
            head->prevNode = newNode;
            newNode->nextNode = head;
            head = newNode;
        }

        sum += value;
        ++count;
        return newNode;
    }

    void LinkedList::addFirst( Node * node )
    {
        if( head == NULL )
        {
            head = node;
            tail = node;
        }
        else
        {
            head->prevNode = node;
            node->nextNode = head;
            head = node;
        }

        sum += node->value;
        ++count;
    }

    string LinkedList::toString()
    {
        string res;
        Node* temp = head;

        res.append( "Sum: " ).append( to_string( sum ) ).append( "\r\n" );
        res.append( "Count: " + to_string( count ) + "\r\n" );

        string sa = "Average: " + to_string( getAverage() );
        res.append( sa.substr( 0, sa.size() - 4 ) ).append( "\r\n" );

        while( temp != NULL )
        {
            if( temp == tail )
            {
                res.append( to_string( temp->value ) + "\r\n" );
            }
            else
            {
                res.append( to_string( temp->value ) + ", " );
            }
            temp = temp->nextNode;
        }

        delete temp;
        return res;
    }

    double LinkedList::getAverage()
    {
        if( count != 0 ) 
        {
            return (double)sum / (double)count;
        }
        return 0;
    }

    void LinkedList::print()
    {
        Node* temp = head;

        while( temp != NULL )
        {
            cout << temp->value << " ";
            temp = temp->nextNode;
        }

        delete temp;
        cout << endl;
    }
