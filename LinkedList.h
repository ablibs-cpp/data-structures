#pragma once
#include <iostream>
#include <stdlib.h>
#include <string>
#include <vector>

using namespace std;

typedef struct NodeStruct
{
    int value;
    NodeStruct* prevNode = NULL;
    NodeStruct* nextNode = NULL;
} Node;

typedef Node* ( *delAddAfter ) ( Node*, int );
typedef Node* ( *delAddLast )( int );

class LinkedList
{
    public:
    LinkedList();
    ~LinkedList();

    /***** Experimental *****/
    Node* addLast( int value );
    void addLast( Node* node );
    /***** End of Experimental *****/
    Node* addAfter( Node* qNode, int value );
    Node* AddBefore( Node* qNode, int value );
    Node* addFirst( int value );
    void addFirst( Node* node );

    void addRange( LinkedList linkedList);
    template <class T> bool contains( T query );
    Node** find( int value );
    Node* findFirst( int value );
    Node* findLast( int value );
    template <class T> void remove( T value );
    void removeFirst();
    void removeLast();
    string toString();
    double getAverage();
    void print();

    private:
    Node* head;
    Node* tail;
    int sum;
    int average;
    int count;

};

template<class T>
inline bool LinkedList::contains( T query )
{
    if( std::is_same<T, NodeStruct*>::value )
    {
        Node* temp = head;
        Node* searchNode = (Node*)query;
        while( temp != NULL )
        {
            if( temp == searchNode )
            {
                return true;
            }
            temp = temp->nextNode;
        }
    }
    else if( std::is_same<T, int>::value )
    {
        Node* temp = head;
        int searchVal = (int)query;
        while(temp != NULL)
        {
            if( temp->value == searchVal )
            {
                return true;
            }
            temp = temp->nextNode;
        }
    }

    return false;
}

template<class T>
inline void LinkedList::remove( T value )
{
    Node* temp = head;
    if( std::is_same<T, NodeStruct*>::value )
    {
        Node* searchNode = (Node*)value;
        while( temp != NULL )
        {
            if( temp == searchNode )
            {
                if( searchNode == head && searchNode == tail )
                {
                    head = NULL;
                    tail = NULL;
                }
                if( searchNode == head )
                {
                    head = head->nextNode;
                    head->prevNode = NULL;
                }
                else if( searchNode == tail )
                {
                    tail = tail->prevNode;
                    tail->nextNode = NULL;
                }
                else
                {
                    searchNode->prevNode->nextNode = searchNode->nextNode;
                    searchNode->nextNode->prevNode = searchNode->prevNode;
                }
                sum -= searchNode->value;
                --count;

                delete temp;
                break;
            }
            temp = temp->nextNode;
        }
    }
    else if( std::is_same<T, int>::value )
    {
        int searchVal = (int)value;
        while( temp != NULL )
        {
            if( temp->value == searchVal )
            {
                if( temp == head && temp == tail )
                {
                    head = NULL;
                    tail = NULL;
                }
                if( temp == head )
                {
                    head = head->nextNode;
                    head->prevNode = NULL;
                }
                else if( temp == tail )
                {
                    tail = tail->prevNode;
                    tail->nextNode = NULL;
                }
                else
                {
                    temp->prevNode->nextNode = temp->nextNode;
                    temp->nextNode->prevNode = temp->prevNode;
                }
                sum -= searchVal;
                --count;

                delete temp;
                break;
            }
            temp = temp->nextNode;
        }
    }
    else
        return;

}
