#include <iostream>
#include "LinkedList.h"
#include "Delegate.h"
#include <string.h>

using namespace std;

typedef int( *SquareFuntionPointer )( int );

template<typename T>
using UniquePtr = std::unique_ptr<T>;

template <typename out, typename... in>
using FPtr = out( * )( in );

int main() {
    UniquePtr<Node> uptr( new Node() );

    FPtr<Node*, int> addLastPtr = static_cast<Node*( * )( int )>(&LinkedList::addLast);

    bool cont = true;
    while( cont )
    {
        
        LinkedList linkedList;
        Node* nodes[9];

        for( int i = 1; i < 10; i++ )
        {
            nodes[i - 1] = linkedList.addLast( i );
        }
        cout << linkedList.toString() << endl;

        linkedList.addLast( 3 );

        auto fpFind = DELEGATE( &LinkedList::find, &linkedList );
        auto fpRemoveFirst = DELEGATE(&LinkedList::removeFirst, &linkedList);
        Delegate<bool, Node*> fpContainsNode = DELEGATE( &LinkedList::contains<Node*>, &linkedList );
        Delegate<bool, int> fpContainsInt= DELEGATE( &LinkedList::contains<int>, &linkedList );
        Delegate<Node*, Node*, int> fpAddBefore = DELEGATE( &LinkedList::AddBefore, &linkedList );


        cout << fpFind( 3 )[0]->prevNode->value << endl;
        cout << fpFind( 3 )[1]->prevNode->value << endl;

        cout << fpContainsNode( nodes[5] ) << endl;
        cout << fpContainsNode( new Node() ) << endl;

        cout << fpContainsInt( 3 ) << endl;
        cout << fpContainsInt( 500 ) << endl;

        cout << "Continue? (Y/N)";
        char* in = new char[0];
        cin >> in;
        if( _stricmp( in, "Y" ) != 0 ) 
        {
            cont = false;
        }
    }
    return 0;
}